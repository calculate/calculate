#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#define endOf(c) c == '(' ? ')' : ']'

#define skip_white while(chr == ' ' || chr == '	' || chr == '\r' || chr == '\n') {nextchr;}

#define SQRT 's'

typedef struct tree tree;
struct tree
{
	tree *l;
	char op;
	tree *r;

	long double val;

	tree *parent;

	char error;
};

int lev(char op) // For calculating the level of priority of the operator
{
	switch(op)
	{
		case '*':
		case '/':
			return 1;
		case '^':
			return 2;
	}
	return 0;
} 

long double tinyCalc(char o, long double v)
{
	if(o == SQRT)
	{
		printf(" | sqrt %LG\n", v);
		return sqrt(v);
	}
	else
	{
		printf("BUG : unknown operation '%c' on line %d\n", o, __LINE__); 
		exit(EXIT_FAILURE);
	}
}

long double calc(long double l, char o, long double r)
{
	printf(" | %LG %c %LG\n", l, o, r);
	switch(o)
	{
		case '+' : return l + r;
		case '-' : return l - r;
		case '*' : return l * r;
		case '/' : return l / r;
		case '^' : return pow(l,r);
		default :
			printf("BUG : unknown operation '%c' on line %d\n", o, __LINE__); 
			exit(EXIT_FAILURE);
	}
}

/*void print_tree(tree *t)
{
	printf
	(
		"Left  : %p\n"
		"Right : %p\n"
		"OP    : %c\n"
		"val   : %LF\n",
		t->l,
		t->r,
		t->op,
		t->val
	);
}
*/

long double eval_tree(tree *t, int free_tree)
{
	if(t->l == NULL) //FIXME : Is that correct ?
	{
		if(free_tree)
		{
			long double val = t->val;
			free(t);
			return val;
		}
		return t->val;
	}
	else if(t->r == NULL)
	{ // in case only one terme was given, like "2" or "(3+4)"
		if(t->op == 0)
		{
			if(free_tree)
			{
				long double val = eval_tree(t->l, free_tree);
				free(t);
				return val;
			}

			return t->val;
		}
		else
		{
			long double val = tinyCalc(t->op, eval_tree(t->l, free_tree));
			if(free_tree)
				free(t);
			return val;
		}
	}

	long double val =  calc (
		eval_tree(t->l, free_tree),
		t->op,
		eval_tree(t->r, free_tree)
	);

	if(free_tree)
		free(t);

	return val;
}

void *secure_malloc(size_t t)
{
	void *p = malloc(t);
	if(!p)
	{
		printf("Allocation error, program is terminated immediatly.\n");
		exit(EXIT_FAILURE); // ciao !
	}
	return p;
}

#define malloc(t) secure_malloc(t)

tree *zerotree()
{ // will return a new initialized tree.
	tree *t = malloc(sizeof(tree));

	t->val = t->error = 0;
	t->r = t->l = t->parent = NULL;

	return t;
}

/*
void repC(int indent)
{
	int i=0;
	while (i < indent)
	{
		printf("	");
		++i;
	}
}

void print_t(tree *t, int indent)
{
	if(t->l)
	{
		repC(indent);
		puts(" {");
		print_t(t->l, indent + 1);
		repC(indent + 1);
		printf("%c (%d)\n", t->op, t->op);

		if(t->r)
			print_t(t->r, indent + 1);
		else
		{
			repC(indent + 1);
			puts("NULL");
		}

		repC(indent);
		puts(" }");
	}
	else
	{
		repC(indent);
		printf("%LG\n", t->val);
	}
}
*/

tree *subtree(char **s, char end)
{
#define ch(x) (*s)[x]
#define chr (*s)[0]
#define nextchr *s+=sizeof(char);
#define skipchr(x) *s+= x * sizeof(char);

	tree
		*tmp = NULL, *tmp2 = NULL, // tmp trees which will be moved to root or t
		*root, // the root of the tree
		*t; // the current tree

	root = t = zerotree();

	char op = 0;
	int H; // tmp bool variable for reuse evaluation of operator priority

	skip_white

	while(chr != end && chr != '\0')
	{
		if(chr == '(' || chr == '[')
		{
			nextchr;
			tmp = subtree(s, endOf(ch(-1)));
			if(tmp->error)
				goto ERROR;
		}
		else if (chr == '+')
		{
			nextchr;
			op = '+';
		}
		else if (chr == '-')
		{
			nextchr;
			op = '-';
		}
		else if (chr == '*')
		{
			nextchr;
			op = '*';
		}
		else if(chr == '/')
		{
			nextchr;
			op = '/';
		}
		else if(chr == '^')
		{
			nextchr;
			op = '^';
		}
		else if (chr == 's' && ch(1) == 'q' && ch(2) == 'r' && ch(3) == 't')
		{
			skipchr(4);
			skip_white;
			tmp = zerotree();

			if(chr == '('  || chr == '[')
			{
				nextchr;
				tmp->l = subtree(s, endOf(ch(-1)));
			}
			else if(chr >= '0' && chr <= '9')
			{ // Parsing number like this may be unreliable.
				tmp->l = zerotree();
				tmp->l->val = strtod(&(chr), s);
			}
			else goto ERROR;

			tmp->op = SQRT;
		}
		else if(chr >= '0' && chr <= '9')
		{ // Parsing number like this may be unreliable.
			tmp = zerotree();
			tmp->val = strtod(&(chr), s);
		}
		else
		{ // unrecognized char
		printf("Unrecognized char: '%c'; next = '%c'; end = '%c'; ", chr, ch(1), end);
			goto ERROR;
		}

skip_parsing:
		if(t->l != NULL && t->r != NULL)
		{
			if(!op)
				op = '*'; // tmp is NOT null, will return to skip_parsing.
			goto handle_operating_priority;
		}
		else if(t->l == NULL)
		{
			if(tmp)
			{ // a terme has been defined
				t->l = tmp;
				tmp = NULL;
			}
			else if(t->parent == NULL) // t is the root, signe '-' or '+' at the begining of the expression
			{
				t->l = zerotree(); // adding a 0 before the operation.
				t->op = op;
				op = 0;
			}
			else
			{
handle_operating_priority:
				if(t->op == 0)
					t->op = '*';

				if( lev(op) > lev(t->op)) // if op > t:op
				{ // we are creating a new tree and moving t->r to it
					tmp2 = zerotree();

					tmp2->l = t->r;
					tmp2->op = op;

					t->r = tmp2;

					tmp2->parent = t;

					t = tmp2;
/*
OP, OP2 and OP3 are the existant operation(s) and the new operation,
 = is the next tree being created
    t:           t:
   OP    ->     OP
  / \          / \
x1  x2       x1  t2:
                OP2
               /  \
              x2  =
*/
				}
				else
				{
					while((H = ( lev(op) <= lev(t->op) )) && t->parent) // while op <= t:op
						t = t->parent;
					if(H && !t->parent)
					{
						root = zerotree();
						root->l = t;
						t->parent = root;
						t = root;
/*
 t=root:      new root:
   OP    ->     OP2
  / \          /   \
x1  x2       OP  t(ex-root):
            / \     / \
          x1  x2  x3  =
*/
					}
					else
					{
						tmp2 = zerotree();

						tmp2->l = t->r;
						t->r->parent = tmp2;

						t->r = tmp2;
						tmp2->parent = t;

						t = tmp2;
/*
    t:            t:
   OP    ->       OP
  / \          /     \
x1  OP2      x1      OP3:
    / \     / \     /  \
                  OP2  =
                 /  \ 
*/
					}
					t->op = op;
				}
				op = 0;
			}
		}
		else //t->r == NULL
		{
			if(op)
			{
				t->op = op;
				op=0;
			}
			else
			{ // if !op, then tmp != NULL
				if(!t->op)
					t->op = '*';
				t->r = tmp;
				tmp = NULL;
			}
		}
/*
		printf("====\n");
		print_t(root, 0);
		printf("====\n\n");
*/
		if(tmp != NULL) // see the line "goto handle_operating_priority;"
			goto skip_parsing;
		skip_white
	}

	if(chr != end)
	{
ERROR:
		root->error = 1;
	}
	nextchr;
	return root;
}

long double math_expr_eval(char *s, int *err)
{
	tree *t;

	t = subtree(&s, '\0');

	if(t->error)
	{
		*err = 1;
		return 0;
	}

	return eval_tree(t, 1);
}

int main (int argc, char **argv)
{
	if (argc < 2)
	{
		puts("You must put an expression to calculate.");
		return 0;
	}

	int err = 0;

	long double result = math_expr_eval(argv[1], &err);

	if(err)
	{
		puts("could not evaluate the expression.");
	}
	else
	{
		printf("= %.16LG\n", result);
	}

	return 0;
}
